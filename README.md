# Extra Bits for my Blog

This repo is a holding pen for things that I reference on my blog.  You
probably want to go [there](https://write.wrestle.town/oddbloke/)
instead.
