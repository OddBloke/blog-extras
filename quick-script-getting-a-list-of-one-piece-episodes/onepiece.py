import csv
import datetime
import re
import sys


def output_lines(f):
    reader = csv.DictReader(f)

    rows = []
    for row in reader:
        if row["number"].startswith("S"):
            # Skip specials
            continue
        row["airdatetime"] = datetime.datetime.strptime(
            row["airdate"], "%d %b %y"
        )
        if "https" in row["title"]:
            # There are some misquoted lines in the CSV, fix those up
            # here
            row["title"] = row["title"].split(",")[0].strip('"')
        # Remove " (25 min)" when present
        row['title'] = re.sub(r' \(\d+ min\)', '', row['title'])
        rows.append(row)
    for line in sorted(rows, key=lambda row: row["airdatetime"]):
        try:
            print(f"{line['number']}: {line['title']} ({line['airdate']})")
        except BrokenPipeError:
            pass


if __name__ == "__main__":
    with open("onepiece.csv") as f:
        output_lines(f)
